import { useContext, useEffect } from "react";
import { io } from "socket.io-client";
import { BASE_URL } from "./Constant";
import {ChatContext} from "./Context/ChatContext";

// export function asyncEmit(eventName:string, data:any) {
//     const {socket} = useContext(ChatContext)
//     return (
//       new Promise(function (resolve, reject) {
//         socket.emit(eventName, data);
//         socket.on(eventName, (result:any) => {
//           socket.off(eventName);
//           resolve(result);
//         });
//       })
//     )
// }

