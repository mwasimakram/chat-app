import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { Headers } from "./Component/Common/Headers";
import { Router } from "./Component/Routes/Router";

import ChatContextProvide from "./Context/ChatContext"
function App() {
  return (
    <ChatContextProvide>
      <Headers />
      <Router />
    </ChatContextProvide>
  );
}

export default App;
