export const SIGN_UP_URL ="/signUp";
export const LOGIN_URL = "/";
export const HOME_PAGE_URL="/home"
export const BASE_URL = "http://localhost:3001";
export const API_PREFIX = "/api/v1/";
export const LOGIN_API_URL = "authorize";
export const SIGNUP_API_URL = "signUp";


///Reducer 

export const SET_USER = "SET_USER";
export const SET_REC_USER = "SET_REC_USER";
export const CURRENT_USER ="CURRENT_USER"
export const LOGOUT = "LOGOUT"
export const SAVE_CHAT = "SAVE_CHAT"
export const NEW_MSG_COUNT = "NEW_MSG_COUNT";
export const USER_LIST = "USER_LIST";
export const SHOW_UNREAD = "SHOW_UNREAD";
//socket method

export const GET_USET_LIST ="getUserList";
export const GET_MSG ="getMsg";
export const SENT_MSG = "sentMsg";
export const SENT_MSG_TO = "sendMsgTo";
export const DISCONNECT ="disconnect";
export const GET_NEW_MSG_COUNT = "getNewMsgCount";
export const UNREAD_MSG = "checkUnread";