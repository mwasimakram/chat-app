import { useForm } from "react-hook-form";
import { Container, Row, Col, Button, Card } from "react-bootstrap";
interface Iprops {
  submitForm: (data: any) => void;
}
export function SignUpForm(props: Iprops) {
  const { submitForm } = props;

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data: any) => submitForm(data);
  return (
    <form onSubmit={handleSubmit(onSubmit)} className="loginForm">
      <div className="">
        <label className="mt-2">First Name</label>
        <input
          className="form-control mt-2"
          type="text"
          {...register("firstName", { required: true })}
          placeholder="Your first name"
          autoComplete="false"
        />
        {errors.firstName && (
          <span className="errorMsg">Field is required</span>
        )}
      </div>
      <div className="">
        <label className="mt-2">Last Name</label>
        <input
          className="form-control mt-2"
          type="text"
          {...register("lastName", { required: true })}
          placeholder="Your last name"
          autoComplete="false"
        />
        {errors.lastName && <span className="errorMsg">Field is required</span>}
      </div>
      <div className="">
        <label className="mt-2">Email</label>
        <input
          className="form-control mt-2"
          type="text"
          {...register("email", { required: true })}
          placeholder="Your last name"
          autoComplete="false"
        />
        {errors.email && <span className="errorMsg">Field is required</span>}
      </div>
      <div>
        <label className="mt-3">Password</label>
        <input
          className="form-control mt-2"
          type="text"
          {...register("password", { required: true, maxLength: 8 })}
          placeholder="Enter your password"
        />
        {errors.password && (
          <span className="errorMsg">Password is required</span>
        )}
      </div>
      <div>
        <label className="mt-3">Confirm Password</label>
        <input
          className="form-control mt-2"
          type="text"
          {...register("password", { required: true, maxLength: 8 })}
          placeholder="Re Enter your password"
        />
        {errors.password && (
          <span className="errorMsg">Password is required</span>
        )}
      </div>
      <Button className="btn btn-success form-control mt-5" type="submit">
        SignUp
      </Button>
    </form>
  );
}
