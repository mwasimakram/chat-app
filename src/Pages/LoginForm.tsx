import { useForm } from "react-hook-form";
import { Container, Row, Col, Button, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import { SIGN_UP_URL } from "../Constant";

interface Iprops {
  submitForm: (data: any) => void;
}
export function LoginForm(props: Iprops) {
  const { submitForm } = props;
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data: any) => submitForm(data);
  return (
    <form onSubmit={handleSubmit(onSubmit)} className="loginForm">
      <div className="">
        <label className="mt-2">Email</label>
        <input
          className="form-control mt-2"
          type="text"
          {...register("email", { required: true })}
          placeholder="Enter your email"
          autoComplete="false"
        />
        {errors.email && (
          <span className="errorMsg">Email is required</span>
        )}
      </div>
      <div>
        <label className="mt-3">Password</label>
        <input
          className="form-control mt-2"
          type="text"
          {...register("password", { required: true, maxLength: 8 })}
          placeholder="Enter your password"
        />
        {errors.password && (
          <span className="errorMsg">Password is required</span>
        )}
      </div>
      <div className="float-end mt-1">
          <Link className="linkClr" to={SIGN_UP_URL}>Don't have an account?</Link>
      </div>
      <Button className="btn btn-success form-control mt-5" type="submit">
        SignIn
      </Button>
    </form>
  );
}
