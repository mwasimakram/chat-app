import { useForm } from "react-hook-form";
const formData =[{register:"username",required:true,type:"email",lable:"UserName"},{register:"password",required:true,type:"password",lable:"Password"}];
interface Iprops{
    handleSubmitForm:(data:any)=>void
}
export function Forms({handleSubmitForm}:Iprops) {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data: any) => handleSubmitForm(data);
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      {formData && formData.map((field,index)=><>
        <label>{field.lable}</label>
        <input type={field.type}{ ...register(field.register ,{required:field.required})}/>
        {errors?.field?.register && <span>This field is required</span>}
      </>)}
      <input type="submit" />
    </form>
  );
}
