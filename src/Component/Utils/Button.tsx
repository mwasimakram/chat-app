import { Button } from "react-bootstrap";
enum ButtonType{
    "button","submit","reset",undefined
}
interface BtnProps {
  text: string;
  variant: string;
  btnType: "button"| "submit" | "reset" | undefined
}
export function Buttons(props: BtnProps) {
  const { text, variant, btnType } = props;
  return (
    <Button variant={variant} type={btnType}>
      {text}
    </Button>
  );
}
