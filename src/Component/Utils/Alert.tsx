import { ToastContainer,toast,Zoom,Bounce } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
interface Iprops{
    msg:string,
}
export function Alert(props:Iprops){
    const {msg,} = props;
    toast.success(msg);
    return(<>
        <ToastContainer  draggable={false} transition={Zoom} autoClose={8000} />
    </>)
}