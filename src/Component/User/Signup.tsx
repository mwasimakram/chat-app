import { Container, Row, Col, Button, Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { signUpUser } from "../../Axios/Services";
import { LOGIN_URL } from "../../Constant";
import { SignUpForm } from "../../Pages/SignUpForm";
import "./User.css"

export function SignUp(){
    const navigate = useNavigate();
    const onSubmit = async (data:any)=> {
      const resp = await signUpUser(data);
      if(resp.success){
        navigate(LOGIN_URL)
      }
    }
    return(
        <Container className="position-absolute top-50 start-50 translate-middle">
      <Row >
        <Col lg={4} md={8} sm={10} xm={12} ></Col>
        <Col lg={4} md={8} sm={10} xm={12} className="  justify-content-center">
          <Card>
            <Card.Body>
              <Card.Title className="text-center">SignUp</Card.Title>
                <SignUpForm submitForm={onSubmit}/>
            </Card.Body>
          </Card>
        </Col>
        <Col lg={4} md={8} sm={10} xm={12} ></Col>
      </Row>
    </Container>
    )
}