import { Container, Row, Col, Button, Card } from "react-bootstrap";
import { Buttons } from "../Utils/Button";
import { LoginForm } from "../../Pages/LoginForm";
import { authorizeUser } from "../../Axios/Services";
import "./User.css";
import { useNavigate } from "react-router-dom";
import { CURRENT_USER, HOME_PAGE_URL } from "../../Constant";
import { ChatContext } from "../../Context/ChatContext";
import { useContext } from "react";
export function Login() {
  const navigate  = useNavigate();
  const {token,setTokenState,socket,dispatch} = useContext(ChatContext)
  const onSubmit = async (data: any) => {
    const resp = await authorizeUser(data);
    if(resp.success){
      socket.emit("addNewUser",{email:resp.data.email,name:resp.data.firstName});
      resp.data.socket_id = socket.id;
      dispatch({type:CURRENT_USER,payload:resp.data});
      dispatch({type:'SAVE_CHAT',payload:[]});
      setTokenState?.(resp.data.firstName);
      navigate(HOME_PAGE_URL);
    }
  }
  return (
    <Container className="position-absolute top-50 start-50 translate-middle">
      <Row >
        <Col lg={4} md={8} sm={10} xm={12} ></Col>
        <Col lg={4} md={8} sm={10} xm={12} className="  justify-content-center">
          <Card>
            <Card.Body>
              <Card.Title className="text-center">Login</Card.Title>
                <LoginForm submitForm={onSubmit}/>
            </Card.Body>
          </Card>
        </Col>
        <Col lg={4} md={8} sm={10} xm={12} ></Col>
      </Row>
    </Container>
  );
}
