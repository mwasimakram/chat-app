import { useEffect } from "react";
import { ListGroup, Row, Col } from "react-bootstrap";

interface ChatHistory{
  msg:string,
  sender:string,
  reciever:string,
  time:string,
}
interface REC_USER{
  email:string,
  name:string,
}
interface CUR_USER{
  firstName:string,
  lastName:string,
  email:string
}
interface Iprops {
  currentUser: CUR_USER;
  recUser: REC_USER;
  chatHistory:(ChatHistory)[]
}

export function ChatDetails(props: Iprops) {
  const { currentUser, recUser ,chatHistory} = props;
  const chat =chatHistory && chatHistory.filter(
    (msg: ChatHistory) =>
     ( currentUser.email === msg.sender && recUser.email === msg.reciever) || (currentUser.email === msg.reciever && recUser.email === msg.sender)
  );

  return (
    <div className="chatBox">
      {chat &&
        chat.map((msg: ChatHistory, index:number) => (
          <Row className="ms-2 me-2 mb-1" key={index.toString()}>
            {currentUser.email === msg.reciever && (
              <Col lg={4} sm={8} xs={12}  className="justify-content-end ">
                <div className="recieveMsg">{msg.msg}</div>
                <div className="text-end timestamp">{msg.time}</div>
              </Col>
            )}
            <Col lg={currentUser.email === msg.reciever ? 4 : 8} xs={currentUser.email === msg.reciever ? 2 : 4} ></Col>
            {currentUser.email !== msg.reciever && (
              <Col lg={4} sm={8} xs={12} className="justify-content-end ">
                <div className="sentMsgBg">{msg.msg}</div>
                <div className="text-end timestamp">
                  <span>{msg.time}</span>
                </div>
              </Col>
            )}
          </Row>
        ))}
    </div>
  );
}
