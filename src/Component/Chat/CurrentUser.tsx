interface Iprops{
    currentUser:any
}
export function CurrentUser(props:Iprops){
    const {currentUser} = props;
    return(
        <div>
            {currentUser && <span>LoggedIn as {currentUser.firstName}</span>}
        </div>
    )
}