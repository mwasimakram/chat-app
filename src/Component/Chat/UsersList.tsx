import { ListGroup } from "react-bootstrap";
interface Iprops {
  userList: any[];
  setRecUser: (user: any) => void;
  currentUser: any;
}
export function UsersList(props: Iprops) {
  const { userList, setRecUser, currentUser } = props;
  return (
    <>
      <h3>Active UserList</h3>

      {userList && userList.length === 0 && (
        <span>No one is currently active</span>
      )}
      <ListGroup className="chatBox">
        {userList &&
          userList.map((obj, index) => (
            <>
              {currentUser && currentUser.email !== obj.email && (
                <ListGroup.Item
                  className="cursor-pointer"
                  key={index.toString()}
                  onClick={() => {
                    setRecUser(obj);
                  }}
                >
                  {obj.name}
                </ListGroup.Item>
              )}
            </>
          ))}
      </ListGroup>
    </>
  );
}
