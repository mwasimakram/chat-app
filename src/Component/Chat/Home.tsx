import { Container, Row, Col } from "react-bootstrap";
import { UsersList } from "./UsersList";
import { Search } from "../Common/Search";
import { ChatDetails } from "./ChatDetails";
import { ChatContext } from "../../Context/ChatContext";
import { useContext, useEffect, useState } from "react";
import { CurrentUser } from "./CurrentUser";
import { useMediaQuery } from "react-responsive";
import "./Chat.css";

import {
  GET_MSG,
  GET_NEW_MSG_COUNT,
  GET_USET_LIST,
  NEW_MSG_COUNT,
  SAVE_CHAT,
  SENT_MSG,
  SENT_MSG_TO,
  SET_REC_USER,
  SET_USER,
  UNREAD_MSG,
} from "../../Constant";
import { UnReadMessage } from "./UnReadMsg";

interface ChatHistory {
  msg: string;
  sender: string;
  reciever: string;
  time: string;
}
interface REC_USER {
  email: string;
  name: string;
}
export function Home() {
  const { socket, state, dispatch, UnReadChat, addToArray } =
    useContext(ChatContext);
  const [typeMsg, setMsg] = useState<string>("");
  const isBigScreen = useMediaQuery({ query: "(min-width: 1020px)" });

  const updateUserList = async (data: string) => {
    dispatch({ type: SET_USER, payload: data });
  };

  socket.on(GET_USET_LIST, updateUserList);

  const setRecUser = (obj: REC_USER) => {
    dispatch({ type: SET_REC_USER, payload: obj });
  };

  socket.on(GET_MSG, (data: ChatHistory[]) => {
    dispatch({ type: SAVE_CHAT, payload: data });
  });

  socket.on(UNREAD_MSG, (data: ChatHistory) => {
    if (!state.recUser) {
      addToArray?.([...UnReadChat, data]);
      showNotification({ sender: data.sender, body: data.msg });
    } 
     if (state.recUser && state.recUser.email !== data.sender) {
      addToArray?.([...UnReadChat, data]);
      showNotification({ sender: data.sender, body: data.msg });
    }
  });

  socket.on(SENT_MSG, (data: ChatHistory) => {
    dispatch({ type: SAVE_CHAT, payload: data });
  });

  function showNotification(obj: any) {
    const notification = new Notification(obj.sender.split("@")[0], {
      body: obj.body,
    });
    notification.close();
  }
  function setRecUserForUnread(email: string) {
    const obj = state.userList.find((user: any) => user.email === email);
    dispatch({ type: SET_REC_USER, payload: obj });
  }
  useEffect(() => {
    if (!("Notification" in window)) {
      Notification.requestPermission();
    }
  }, []);

  return (
    <Container>
      <Row className="mt-2">
        <Col lg={3} md={2} sm={4} xs={4}>
          {isBigScreen && (
            <UsersList
              userList={state.userList}
              setRecUser={setRecUser}
              currentUser={state.currentUser}
            />
          )}
        </Col>
        <Col lg={4} md={6} sm={12} xs={12}>
          <div className="d-flex">
            <h3>
              Inbox <span>({state.recUser && state.recUser.name})</span>
            </h3>
          </div>
          {state.recUser && (
            <ChatDetails
              currentUser={state.currentUser}
              recUser={state.recUser}
              chatHistory={state.chatHistory}
            />
          )}
          <div className="d-flex mt-2">
            <Search
              type="text"
              placeHolder="Type your message"
              setValue={(val) => {
                setMsg(val);
              }}
            />

            <button
              className="btn btn-success ms-2"
              onClick={() => {
                socket.emit(SENT_MSG_TO, {
                  user: state.recUser,
                  msg: typeMsg,
                  sender: state.currentUser.email,
                });
              }}
            >
              Send
            </button>
          </div>
        </Col>
        <Col lg={2}>
          <CurrentUser currentUser={state.currentUser} />
        </Col>
        <Col lg={3} md={6} sm={8} xs={8}>
          {state.showUnRead && (
            <UnReadMessage setRecUserForUnread={setRecUserForUnread} />
          )}
        </Col>
      </Row>
    </Container>
  );
}
