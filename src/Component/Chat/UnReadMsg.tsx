import { useContext } from "react";
import { ListGroup } from "react-bootstrap";
import { ChatContext } from "../../Context/ChatContext";

interface Iprops {
  setRecUserForUnread: (user: any) => void;
}

export function UnReadMessage(props: Iprops) {
  const { setRecUserForUnread } = props;
  const { UnReadChat } = useContext(ChatContext);

  return (
    <ListGroup>
      {UnReadChat &&
        UnReadChat.map((msg: any, index: number) => (
          <ListGroup.Item
            className="cursor-pointer"
            key={index.toString()}
            onClick={() => {
              setRecUserForUnread(msg.sender);
            }}
          >
            From:<strong>{msg.sender.split("@")[0]}:</strong> {msg.msg}
          </ListGroup.Item>
        ))}
    </ListGroup>
  );
}
