import { setSourceMapRange } from "typescript";

interface Iprops {
  type: string;
  value?: string;
  placeHolder?: string;
  setValue?: (value: string) => void;
}
export function Search({ type, value, placeHolder, setValue }: Iprops) {
  const setInputValue = (event: any) => {
    setValue?.(event.target.value);
  };
  return (
    <input
      className="form-control"
      type={type}
      placeholder={placeHolder}
      value={value}
      onChange={setInputValue}
    ></input>
  );
}
