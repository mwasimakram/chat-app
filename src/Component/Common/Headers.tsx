import { useContext, useEffect, useState } from "react";
import {
  Container,
  Navbar,
  Offcanvas,
  Nav,
  Badge,
  NavDropdown,
} from "react-bootstrap";
import { useMediaQuery } from "react-responsive";
import { NavLink } from "react-router-dom";
import {
  DISCONNECT,
  GET_NEW_MSG_COUNT,
  HOME_PAGE_URL,
  LOGIN_URL,
  LOGOUT,
  SET_REC_USER,
  SIGN_UP_URL,
} from "../../Constant";
import { ChatContext } from "../../Context/ChatContext";
import { Navigate } from "react-router-dom";
import { BsFillBellFill } from "react-icons/bs";
import { UsersList } from "../Chat/UsersList";
import "./Common.css";
import { UnReadMessage } from "../Chat/UnReadMsg";
interface MsgCount {
  total: number;
}
interface REC_USER {
  email: string;
  name: string;
}
export function Headers() {
  
  let activeStyle = {
    textDecordation: "underline",
    color: "white",
  };

  const { socket, state, dispatch, UnReadChat, addToArray } = useContext(ChatContext);
  const isSmallScreen = useMediaQuery({ query: "(max-width: 420px)" });
  const firstName = state.currentUser && state.currentUser.firstName;
  const [showUnRead,setUnRead] = useState<boolean>(false);
  function logOut() {
    dispatch({ type: LOGOUT, payload: {} });
    socket.emit(DISCONNECT);
    <Navigate to={LOGIN_URL} replace={true} />;
  }

  const setRecUser = (obj: REC_USER) => {
    dispatch({ type: SET_REC_USER, payload: obj });
  };

  useEffect(() => {
    let remaining = UnReadChat.filter(
      (msg: any) => msg.sender !== state.recUser.email
    );
    addToArray?.(remaining);
  }, [state.recUser]);


  return (
    <Navbar bg="dark" expand="lg" variant="dark">
      <Container>
        <Navbar.Brand href="#home">Chat App</Navbar.Brand>
        <Navbar.Toggle aria-controls="offcanvasNavbar-expand-false" />
        {/* <Navbar.Collapse id="basic-navbar-nav">
          </Navbar.Collapse> */}
        <Navbar.Offcanvas
          id="offcanvasNavbar-expand-false"
          // eslint-disable-next-line jsx-a11y/aria-props
          aria-labeledby="offcanvasNavbarLable-expand-false"
          placement="end"
        >
          <Offcanvas.Header closeButton>
            <Offcanvas.Title id="offcanvasNavbarLabel-expand-false">
              Menu
            </Offcanvas.Title>
          </Offcanvas.Header>
          <Offcanvas.Body className="bg-dark">
            {firstName ? (
              <Nav className="justify-content-end flex-grow-1 pe-3">
                <NavLink end to="#" className="me-2">
                  <BsFillBellFill onClick={()=>{dispatch({type:"SHOW_UNREAD",payload:false})}}></BsFillBellFill>{" "}
                  <Badge bg="primary" className="notification-badge">
                    {(UnReadChat && UnReadChat.length) || 0}
                  </Badge>
                </NavLink>
                <span style={{ color: "white" }}>{firstName}</span>
                <NavLink
                  className="ms-2"
                  end
                  to={LOGIN_URL}
                  style={({ isActive }) =>
                    isActive ? activeStyle : { color: "gray" }
                  }
                  onClick={() => logOut()}
                >
                  LogOut
                </NavLink>
              </Nav>
            ) : (
              <Nav className="justify-content-end flex-grow-1 pe-3">
                <NavLink
                  className="me-2"
                  end
                  to={LOGIN_URL}
                  style={({ isActive }) =>
                    isActive ? activeStyle : { color: "gray" }
                  }
                >
                  SingIn
                </NavLink>
                <NavLink
                  end
                  to={SIGN_UP_URL}
                  style={({ isActive }) =>
                    isActive ? activeStyle : { color: "gray" }
                  }
                >
                  SignUp
                </NavLink>
              </Nav>
            )}
            {isSmallScreen && (
              <UsersList
                userList={state.userList}
                setRecUser={setRecUser}
                currentUser={state.currentUser}
              />
            )}
          </Offcanvas.Body>
        </Navbar.Offcanvas>
      </Container>
    </Navbar>
  );
}
