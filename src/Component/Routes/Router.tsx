import { Routes, Route } from "react-router";
import { Home } from "../Chat/Home";
import { Login } from "../User/Login";
import { SignUp } from "../User/Signup";
import { ProtectedRoute } from "./ProtectedRoutes";
export function Router() {
  return (
    <Routes>
      <Route path="/" element={<Login />}></Route>
      <Route path="/signUp" element={<SignUp />}></Route>
      <Route
          path="home"
          element={
            <ProtectedRoute>
              <Home />
            </ProtectedRoute>
          }
        />
    </Routes>
  );
}
