import { useContext } from "react";
import { Navigate } from "react-router-dom";
import { ChatContext } from "../../Context/ChatContext";
export function ProtectedRoute(props:any){
    const {children} = props;
    const {token} = useContext(ChatContext)
    if (!token) {
      return <Navigate to="/" replace />;
    }
  
    return children;
};

