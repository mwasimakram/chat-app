import { CURRENT_USER, LOGOUT, NEW_MSG_COUNT, SAVE_CHAT, SET_REC_USER, SET_USER, UNREAD_MSG, SHOW_UNREAD } from "../Constant";
interface Action{
    type:string,
    payload:any
}

export function ChatReducer(state :any,action:Action){
    const {type,payload} =  action;
    switch(type){
        case SET_USER:
            return{
                ...state,
                userList:payload
            }
        case SET_REC_USER:
            return{
                ...state,
                recUser:payload
            }
        
        case CURRENT_USER:
            return{
                ...state,
                currentUser:payload
            }
        case LOGOUT:
            return{
                payload
            }
        case SAVE_CHAT:
            return{
                ...state,
                chatHistory: payload
            }
        case NEW_MSG_COUNT:{
            return{
                ...state,
                newMsgCount:payload
            }
        }
        case UNREAD_MSG:
            return{
                ...state,
                unreadMsg:payload

            }
        case SHOW_UNREAD:{
            return {
                ...state,
                showUnRead: state.showUnRead && state.showUnRead === true ? false :true   || true
            }
        }
        
    }
}

