import axios from "axios";
axios.interceptors.request.use((request:any)=>{
  const token = localStorage.getItem('access_token');
  if(!request.url.includes("/")){
    request.headers.common.Authorization = `Bearer ${token}`;
  }
  return request;
});
export async function GetReq(url: string,params={}) {
  try {
    const config = {
      method: "get",
      url: `${url}`,
      params:params
    };
    return await axios(config);
  } catch (error) {
    return error;
  }
}
export async function PostReq(url: string,body={}) {
    try {
      const config = {
        method: "post",
        url: `${url}`,
        data:body
      };
      return await axios(config);
    } catch (error) {
      return error;
    }
}