import { GetReq,PostReq } from "./Axios";
import { BASE_URL,API_PREFIX,LOGIN_API_URL, SIGNUP_API_URL } from "../Constant";
export async function authorizeUser(params={}){
    try {
        let result:any;
        result = await GetReq(`${BASE_URL}${API_PREFIX}${LOGIN_API_URL}`,params);
        return result.data;
    } catch (error) {
        throw error;
    }
}
export async function signUpUser(body={}){
    try {
        let result:any;
        result = await PostReq(`${BASE_URL}${API_PREFIX}${SIGNUP_API_URL}`,body);
        return result.data;
    } catch (error) {
        throw error;
    }
}
