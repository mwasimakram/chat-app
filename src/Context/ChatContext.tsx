import { createContext, useReducer, useState } from "react";
import { propTypes } from "react-bootstrap/esm/Image";
import { ChatReducer } from "../Reducer/ChatReducer";
import { io } from "socket.io-client";
import { BASE_URL } from "../Constant";
const socket = io(BASE_URL);

interface IContext{
    token:string,
    setTokenState?:(token:string)=>void,
    socket:any,
    state:any,
    dispatch: React.Dispatch<any>;
    addToArray?:(obj:any) => void;
    UnReadChat:any

}
const initialState = { chatHitory: [],userList:[],recUser:{} ,currentUser:{},newMsgCount:0,unreadMsg:[]}

const defaultState={
    token:' ',
    socket:{},
    state:{},
    dispatch: () => null,
    UnReadChat:[]
}
export const ChatContext = createContext<IContext>(defaultState);

const ChatContextProvide = (props:any)=>{
    const [token,setToken] = useState(defaultState.token);
    const [state,dispatch] = useReducer(ChatReducer,[]);
    const [UnReadChat,addToArray] = useState([])
    function setTokenState(token:string){
        setToken(token)
    }
   
    return(
        <ChatContext.Provider value={{token,setTokenState,socket,state,dispatch,addToArray,UnReadChat}}>
            {props.children}
        </ChatContext.Provider>
    )
}

export default ChatContextProvide;